import { prisma } from './prisma';

async function main() {
  await prisma.$connect();

  const users = await prisma.user.findMany({
    include: {
      orders: true,
    },
  });

  console.log(users);
}

main();
